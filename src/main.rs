use std::io::{self, Write};
use std::fs::{File, OpenOptions};
use std::process::{Command};

fn main() {
    loop {
        let cmd: String;
        display_prompt();
        cmd = read_command();
        parse_command(&cmd);
    }
}

fn display_prompt() {
    print!("$> ");
    io::stdout().flush().unwrap();
}

fn read_command() -> String {
    let mut input = String::new();

    let cmd = match io::stdin().read_line(&mut input) {
        Ok(_) => input,
        Err(_) => String::new(),
    };

    cmd
}

fn parse_command(input_line: &str) {
    let mut input_line = input_line.split_whitespace();
    let mut cmd_line: Vec<&str> = Vec::new();

    let mut stdout_file: Option<File> = None;
    let mut stderr_file: Option<File> = None;

    loop {
        let mut temp = &mut input_line;
        let item: &str;
        if let Some(element) = temp.next() {
            item = element;
        }
        else {
            break
        }

        match item {
            ">" | "1>" => {
                if let Some(filename) = temp.next() {
                    if let Ok(file) = OpenOptions::new().create(true).write(true).truncate(true).open(filename) {
                        stdout_file = Some(file)
                    }
                }
                else {
                    println!("Rush: Syntax Error: Expected filename after operator '>'");
                    return
                };
            },            
            ">>" | "1>>" => {
                if let Some(filename) = temp.next() {
                    if let Ok(file) = OpenOptions::new().create(true).write(true).append(true).open(filename) {
                        stdout_file = Some(file)
                    }
                }
                else {
                    println!("Rush: Syntax Error: Expected filename after operator '>>'");
                    return
                };
            },            
            "2>" => {
                if let Some(filename) = temp.next() {
                    if let Ok(file) = OpenOptions::new().create(true).write(true).truncate(true).open(filename) {
                        stderr_file = Some(file)
                    }
                }
                else {
                    println!("Rush: Syntax Error: Expected filename after operator '>'");
                    return
                };
            },            
            "2>>" => {
                if let Some(filename) = temp.next() {
                    if let Ok(file) = OpenOptions::new().create(true).write(true).append(true).open(filename) {
                        stderr_file = Some(file)
                    }
                }
                else {
                    println!("Rush: Syntax Error: Expected filename after operator '>>'");
                    return
                };
            },
            _ => cmd_line.push(item),
        }
    }
    /*
            //"2>&1" => ,
            //"<" => ,
            //"|" => ,
            _ => cmd_line.push(item),
        }
    }
    */

    if let Ok(mut command) = create_command(cmd_line) {
        if let Ok(output) = command.output() {
            if let Some(mut stdout) = stdout_file {
                stdout.write_all(&output.stdout).expect("Rush: Error: Unable to write to stdout file");
            }
            else {
                print!("{}", String::from_utf8_lossy(&output.stdout));
            }

            if let Some(mut stderr) = stderr_file {
                stderr.write_all(&output.stderr).expect("Rush: Error: Unable to write to stderr file");
            }
            else {
                print!("{}", String::from_utf8_lossy(&output.stderr));
            }
            println!("");
        }
        else {
            println!("Rush: Error: unable to start command");
        }    
    }
    else {
        println!("Rush: Error: unable to create command");
    }    
}

fn create_command(cmd_line: Vec<&str>) -> Result<Command, &'static str> {
    let mut cmd_iter = cmd_line.iter();

    let mut cmd = match cmd_iter.next() {
        Some(bin) => Command::new(bin),
        None => return Err("bad command format"),
    };

    for arg in cmd_iter {
        cmd.arg(arg);
    }

    Ok(cmd)
}

