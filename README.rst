###################
Rush - A Rust shell
###################

Rush aim to implement a unix shell in 
`Rust <https://www.rust-lang.org/en-US/>`_ while learning it.

Status
======

0.2
---

* Flux redirection to files know working (">", "1>", ">>", "1>>", "2>" and
  "2>>" operators)

0.1
---

* Can launch commands and print their return
* Doesn't explode if command are returning with error status

Roadmap
=======

Some planned features :

* Command autocompletion
* Nice prompt configuration
* Windows compatibility
* Testing

License
=======

Rush is licensed under the terms of both the CeCILL 2.1 license and the GPLv3.

